-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: zonasaves
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tont_aves`
--

DROP TABLE IF EXISTS `tont_aves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tont_aves` (
  `CDAVE` int(11) NOT NULL AUTO_INCREMENT,
  `DSNOMBRE_COMUN` varchar(100) DEFAULT NULL,
  `DSNOMBRE_CIENTIFICO` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`CDAVE`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tont_aves`
--

LOCK TABLES `tont_aves` WRITE;
/*!40000 ALTER TABLE `tont_aves` DISABLE KEYS */;
INSERT INTO `tont_aves` VALUES (2,'fragata roja','mundus'),(13,'fragata negra','fragata colombius'),(14,'colibri','morado');
/*!40000 ALTER TABLE `tont_aves` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tont_aves_pais`
--

DROP TABLE IF EXISTS `tont_aves_pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tont_aves_pais` (
  `CDPAIS` int(11) NOT NULL,
  `CDAVE` int(11) NOT NULL,
  PRIMARY KEY (`CDPAIS`,`CDAVE`),
  KEY `FK_AVES_idx` (`CDAVE`),
  CONSTRAINT `FK_AVES` FOREIGN KEY (`CDAVE`) REFERENCES `tont_aves` (`CDAVE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PAIS` FOREIGN KEY (`CDPAIS`) REFERENCES `tont_paises` (`CDPAIS`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tont_aves_pais`
--

LOCK TABLES `tont_aves_pais` WRITE;
/*!40000 ALTER TABLE `tont_aves_pais` DISABLE KEYS */;
INSERT INTO `tont_aves_pais` VALUES (1,2),(1,13);
/*!40000 ALTER TABLE `tont_aves_pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tont_paises`
--

DROP TABLE IF EXISTS `tont_paises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tont_paises` (
  `CDPAIS` int(11) NOT NULL AUTO_INCREMENT,
  `DSNOMBRE` varchar(45) DEFAULT NULL,
  `DSZONA` int(11) DEFAULT NULL,
  PRIMARY KEY (`CDPAIS`),
  KEY `DSZONA_idx` (`DSZONA`),
  CONSTRAINT `FK_IDZONA` FOREIGN KEY (`DSZONA`) REFERENCES `tont_zonas` (`CDZONA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tont_paises`
--

LOCK TABLES `tont_paises` WRITE;
/*!40000 ALTER TABLE `tont_paises` DISABLE KEYS */;
INSERT INTO `tont_paises` VALUES (1,'Brasil',1);
/*!40000 ALTER TABLE `tont_paises` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tont_zonas`
--

DROP TABLE IF EXISTS `tont_zonas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tont_zonas` (
  `CDZONA` int(11) NOT NULL,
  `DSNOMBRE` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CDZONA`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tont_zonas`
--

LOCK TABLES `tont_zonas` WRITE;
/*!40000 ALTER TABLE `tont_zonas` DISABLE KEYS */;
INSERT INTO `tont_zonas` VALUES (1,'TROPICAL'),(2,'INVIERNO');
/*!40000 ALTER TABLE `tont_zonas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-21 11:02:29
