import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import {MatRadioModule} from '@angular/material/radio';
import{HttpClient} from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
    results: string[];
    model = {
    nombreCi: "", 
    nombreComun: "",
    nombreC: "",
    deleteAve: "",
    zona: ""               
  }

  constructor(private http: HttpClient) {

  }
  
  ngOnInit(): void {

	this.nameForm = new FormGroup ({
      zona: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'
      }),
      nombreC: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'
      }),
      nombreCi: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'
      }),
      nombreComun: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'
      }),
      deleteAve: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'
      }),
      placa: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'
      })
      language: new FormControl()
    });

   this.http.get('http://localhost:8888/api/consultaraves').subscribe(data => {
   console.log(data);
   this.vehiculos=data.listAves;
   this.myDataSource = new MatTableDataSource();
   this.myDataSource.data = data.listAves;


   })
  }

 onSubmit(){
 this.http.post('http://localhost:8888/api/registrarave', {
      "nombrec": this.model.nombreC,
      "nombreci": this.model.nombreCi
    },{ responseType: 'text' })
      .subscribe(
        (data:any) => {
           alert(data)
           location.reload();
        }
      );
   }

   onSalir(){

   this.http.post('http://localhost:8888/api/filtrarAve',this.model.nombreComun)
      .subscribe(
        res => {
          this.array = "";
          for (let i = 0; i < res.listAves.length; i++) {
          this.array = this.array + "/" + res.listAves[i].dsNombreCientifico
        }
        alert("Resultado de consulta:" + this.array)
        location.reload();
        }
      );
   }

 onClick(){

 this.http.post('http://localhost:8888/api/salidavehiculo',this.model.placaSalida)
      .subscribe(
        res => {
          if(res.valor != 0 ){
            alert("Mensaje:" + res.mensaje + " " + "El valor a pagar es de:" +  res.valor)
          }else{
            alert("Mensaje:" + res.mensaje + " ")
          }
          location.reload();
        }
      );
  

   }

  onDelete(){

  this.http.post('http://localhost:8888/api/eliminarAve',this.model.deleteAve)
      .subscribe(
        res => {
            alert(res.mensaje)         
          location.reload();
        }
      );
  

   }


  }

}
