package com.co.IAS.restaves;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class RestavesApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestavesApplication.class, args);
	}
}
