package com.co.IAS.restaves.bean;

/**
 * Clase de Mapeo de la informacion de un aves
 * @author: Angie martinez
 */
public class AveMap {
	
	private String nombrec;
	private String nombreci;
	
	/**
	 * @return the nombrec
	 */
	public String getNombrec() {
		return nombrec;
	}
	/**
	 * @param nombrec the nombrec to set
	 */
	public void setNombrec(String nombrec) {
		this.nombrec = nombrec;
	}
	/**
	 * @return the nombreci
	 */
	public String getNombreci() {
		return nombreci;
	}
	/**
	 * @param nombreci the nombreci to set
	 */
	public void setNombreci(String nombreci) {
		this.nombreci = nombreci;
	}

	
}
