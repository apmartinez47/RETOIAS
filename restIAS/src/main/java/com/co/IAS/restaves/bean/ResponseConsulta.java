package com.co.IAS.restaves.bean;

import java.util.List;

import com.co.IAS.restaves.model.Aves;

/**
 * Clase de Mapeo del response del Rest de Consulta de ave
 * @author: Angie Martinez
 */
public class ResponseConsulta {
	
	private List<Aves> listAves;
	private String mensaje;
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public List<Aves> getListAves() {
		return listAves;
	}
	public void setListAves(List<Aves> listAves) {
		this.listAves = listAves;
	}

}
