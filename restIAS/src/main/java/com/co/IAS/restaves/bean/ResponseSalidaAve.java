package com.co.IAS.restaves.bean;

/**
 * Clase de Mapeo del response del Rest
 * @author: Angie Martinez
 */
public class ResponseSalidaAve {
	
	private int valor;
	private String mensaje;
	
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
