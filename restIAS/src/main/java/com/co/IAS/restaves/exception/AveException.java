package com.co.IAS.restaves.exception;


/**
 * Clase AvesException que maneja las excepciones en los flujos de gestion del parqueadero 
 * @author: Angie Martinez
 */
public class AveException extends Exception {
	
	private static final long serialVersionUID = 314493051380955826L;

		public AveException(String message) {
	        super(message);
	    }
	

}
