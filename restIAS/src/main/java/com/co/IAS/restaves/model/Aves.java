package com.co.IAS.restaves.model;

import java.io.Serializable;
import javax.persistence.*;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * Clase de persistencia para la tabla de tont_aves de la base de datos.
 */
@Entity
@Table(name="tont_aves")
@EntityListeners(AuditingEntityListener.class)
public class Aves implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name="cdave")
	private int cdAve;
	
	@Column(name="dsnombre_comun")
	private String dsNombreComun;
	
	@Column(name="dsnombre_cientifico")
	private String dsNombreCientifico;

	/**
	 * @return the cdAve
	 */
	public int getCdAve() {
		return cdAve;
	}

	/**
	 * @param cdAve the cdAve to set
	 */
	public void setCdAve(int cdAve) {
		this.cdAve = cdAve;
	}

	/**
	 * @return the dsNombreComun
	 */
	public String getDsNombreComun() {
		return dsNombreComun;
	}

	/**
	 * @param dsNombreComun the dsNombreComun to set
	 */
	public void setDsNombreComun(String dsNombreComun) {
		this.dsNombreComun = dsNombreComun;
	}

	/**
	 * @return the dsNombreCientifico
	 */
	public String getDsNombreCientifico() {
		return dsNombreCientifico;
	}

	/**
	 * @param dsNombreCientifico the dsNombreCientifico to set
	 */
	public void setDsNombreCientifico(String dsNombreCientifico) {
		this.dsNombreCientifico = dsNombreCientifico;
	}

	
}