package com.co.IAS.restaves.model;

import java.io.Serializable;
import javax.persistence.*;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;


/**
 * Clase de persistencia para la tabla de tont_zonas de la base de datos.
 */
@Entity
@Table(name="tont_paises")
@EntityListeners(AuditingEntityListener.class)
public class Paises implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="cdpais")
	private int cdPais;
	
	@Column(name="dsnombre")
	private String dsNombre;

	@Column(name="dszona")
	private int dsZona;

	/**
	 * @return the cdPais
	 */
	public int getCdPais() {
		return cdPais;
	}

	/**
	 * @param cdPais the cdPais to set
	 */
	public void setCdPais(int cdPais) {
		this.cdPais = cdPais;
	}

	/**
	 * @return the dsNombre
	 */
	public String getDsNombre() {
		return dsNombre;
	}

	/**
	 * @param dsNombre the dsNombre to set
	 */
	public void setDsNombre(String dsNombre) {
		this.dsNombre = dsNombre;
	}

	/**
	 * @return the dsZona
	 */
	public int getDsZona() {
		return dsZona;
	}

	/**
	 * @param dsZona the dsZona to set
	 */
	public void setDsZona(int dsZona) {
		this.dsZona = dsZona;
	}

}