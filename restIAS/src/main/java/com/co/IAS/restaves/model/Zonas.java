package com.co.IAS.restaves.model;

import java.io.Serializable;
import javax.persistence.*;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;


/**
 * Clase de persistencia para la tabla de tont_zonas de la base de datos.
 */
@Entity
@Table(name="tont_zonas")
@EntityListeners(AuditingEntityListener.class)
public class Zonas implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="cdzona")
	private int cdZona;
	
	@Column(name="dsnombre")
	private String dsNombre;

	/**
	 * @return the cdZona
	 */
	public int getCdZona() {
		return cdZona;
	}

	/**
	 * @param cdZona the cdZona to set
	 */
	public void setCdZona(int cdZona) {
		this.cdZona = cdZona;
	}

	/**
	 * @return the dsNombre
	 */
	public String getDsNombre() {
		return dsNombre;
	}

	/**
	 * @param dsNombre the dsNombre to set
	 */
	public void setDsNombre(String dsNombre) {
		this.dsNombre = dsNombre;
	}
			
}