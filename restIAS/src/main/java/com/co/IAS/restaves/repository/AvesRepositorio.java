package com.co.IAS.restaves.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.co.IAS.restaves.model.Aves;

/**
 * Interface del Repositorio de JpaRepositoy para el acceso a los datos del parqueadero en BD
 * @author: Angie Martinez
 */
@Repository												     
public interface AvesRepositorio extends JpaRepository<Aves, Integer>{
	/**
	 * Query que consulta por nombre comun
	 */
	@Query(value = "SELECT * FROM tont_aves WHERE dsnombre_comun = ?1", nativeQuery = true) 
	Aves buscarAveNombreComun(String nombre);
	
	/**
	 * Query que consulta por nombre y zona
	 */
	@Query(value = "SELECT a.dsnombre_comun FROM tont_zonas c \n" + 
			"INNER JOIN tont_paises p \n" + 
			"INNER JOIN tont_aves a \n" + 
			"where c.cdzona = ?1 and" +
			"(a.DSNOMBRE_COMUN LIKE ?2 or a.dsnombre_cientifico LIKE ?2)", nativeQuery = true) 
	List<Aves> buscarAveNombreComun(String nombre, String Zona);
}
