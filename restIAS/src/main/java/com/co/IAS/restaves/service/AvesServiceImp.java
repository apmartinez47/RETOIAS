package com.co.IAS.restaves.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.co.IAS.restaves.bean.AveMap;
import com.co.IAS.restaves.bean.ResponseConsulta;
import com.co.IAS.restaves.bean.ResponseSalidaAve;
import com.co.IAS.restaves.model.Aves;
import com.co.IAS.restaves.repository.AvesRepositorio;
import com.google.gson.Gson;

/**
 * Clase para las validaciones del Rest
 * 
 * @author: Angie Martinez
 */
@Transactional
@Service
public class AvesServiceImp implements AvezService {

	private static final Logger logger = LoggerFactory.getLogger(AvesServiceImp.class);

	
	AvesRepositorio avesRepositorio;

	@Autowired
	public AvesServiceImp(AvesRepositorio avesRepositorio ) {
		super();
		this.avesRepositorio = avesRepositorio;
	}

	/**
	 * Implementacion del Servicio de Registro de aves
	 * 
	 * @param aveJson
	 *            String que contiene el request enforma de Json del ave que
	 *            sera insertado.
	 */
	@Override
	public String registrarAve(String aveJson) {
		Gson gson = new Gson();
		AveMap avesMap = gson.fromJson(aveJson, AveMap.class);
		try {
			Aves ave = new Aves();
			ave.setDsNombreCientifico(avesMap.getNombreci());
			ave.setDsNombreComun(avesMap.getNombrec());
			System.out.println(ave + "sgsgdg");
			
			avesRepositorio.save(ave);
			return "Transacción correcta";
		} catch (Exception e) {
			logger.error(e.toString());
			return e.getMessage();
		}

	}

	/**
	 * Implementacion del Servicio Eliminar de ave
	 * 
	 * @param placa
	 *            String que contiene el nombre comun de la ave que 
	 *            sera consultado para obtener id y eliminar
	 */
	@Override
	public ResponseSalidaAve eliminarAve(String aveCod) {
		ResponseSalidaAve respSalida = new ResponseSalidaAve();
		
		Aves listAves = avesRepositorio.buscarAveNombreComun(aveCod);
		try {
			
			avesRepositorio.delete(listAves.getCdAve());
			respSalida.setMensaje("Transacción correcta");
			
		} catch (Exception e) {
			logger.error("ERROR" + e);
			respSalida.setValor(0);
			respSalida.setMensaje(e.getMessage());
		}
		return respSalida;
	}

	/**
	 * Implementacion del Servicio Consultar todos las aves
	 */
	@Override
	public ResponseConsulta consultarAves() {
		ResponseConsulta respConsulta = new ResponseConsulta();
		try {
			List<Aves> listAves = avesRepositorio.findAll();
			respConsulta.setListAves(listAves);
			respConsulta.setMensaje("Transacción correcta");
		} catch (Exception e) {
			logger.error(e.toString());
			respConsulta.setMensaje("Transacción incorrecta");
		}
		return respConsulta;
	}
	
	/**
	 * Implementacion del Servicio Consultar por nombre y zona
	 */
	@Override
	public ResponseConsulta filtrarAve(String nombre,String zona) {
		ResponseConsulta respConsulta = new ResponseConsulta();
		try {
		
			List<Aves> listAves = avesRepositorio.buscarAveNombreComun(nombre,zona);
			respConsulta.setListAves(listAves);
			respConsulta.setMensaje("Transacción correcta");
		} catch (Exception e) {
			logger.error(e.toString());
			respConsulta.setMensaje("Transacción incorrecta");
		}
		return respConsulta;
	}

}
