package com.co.IAS.restaves.service;

import com.co.IAS.restaves.bean.ResponseConsulta;
import com.co.IAS.restaves.bean.ResponseSalidaAve;

/**
 * Interface de la capa de servicio del Aves
 * @author: Angie Martinez
 */
public interface AvezService {
	
	public String registrarAve(String ave);
	
	public ResponseSalidaAve eliminarAve(String ave);
		
	public ResponseConsulta consultarAves();
	
	public ResponseConsulta filtrarAve(String nombre, String zona);

}
