package com.co.IAS.restparqueadero.rest;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.co.IAS.restaves.bean.ResponseConsulta;
import com.co.IAS.restaves.bean.ResponseSalidaAve;
import com.co.IAS.restaves.service.AvezService;

/**
 * Servicios Rest de gestion de zonas avess
 * @author: Angie Martinez
 */
@RestController
@RequestMapping("/api")
public class AvesRest {
	
	@Autowired
	private AvezService avesService;

	/**
     * Rest de Ingreso de Aves
     * @param vehiculo String que contiene el request enforma de Json del
     * ave que sera insertado.
     * @return String que contiene el mensaje de exito o error del ingreso 
     * del ave
     */
	@CrossOrigin
	@PostMapping("/registrarave")
	public String registrarave(@Valid @RequestBody String ave) {
		return avesService.registrarAve(ave);
	}
	/**
     * Rest de Eliminar Ave
     * @param placa String que contiene la placa del vehiculo que saldra del parqueadero
     * @return ResponseSalidaVehiculo Objeto que contiene el mensaje del proceso
     * de salida del vehiculo y el valor a pagar
     */	
	@CrossOrigin
	@PostMapping("/eliminarAve")
	public ResponseSalidaAve eliminarAve(@Valid @RequestBody String ave) {
	    return avesService.eliminarAve(ave);
	    
	}
	
	
	/**
     * Rest de Consulta de Vehiculos
     * @return ResponseConsulta Objeto que contiene el mensaje del proceso
     * de consulta de vehiculos y la lista de vehiculos en el parqueadero
     */
	@CrossOrigin
	@GetMapping("/consultaraves")
	public ResponseConsulta consultaraves() {
	    return avesService.consultarAves();
	    
	}
	
	/**
     * Rest de Consulta de Aves
     * @return ResponseConsulta Objeto que contiene el mensaje del proceso
     * de consulta de aves y la lista de aves
     */
	@CrossOrigin
	@PostMapping("/filtrarAve")
	public ResponseConsulta filtrarAve(@Valid @RequestBody String nombre, String zona) {
	    return avesService.filtrarAve(nombre,zona);
	    
	}
	
	
}
